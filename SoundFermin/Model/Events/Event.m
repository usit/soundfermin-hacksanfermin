//
//  Event.m
//  SoundFermin
//
//  Created by Usue on 4/6/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import "Event.h"

#define UD_FAVS     @"UDfavs"
#define UD_ALERTS   @"UDalerts"

@implementation Event

-(id)initWithDictionary:(NSDictionary*)dict
{
    if (self = [self init])	{
        self.id_event = [dict objectForKey:EVENT_ID];
        self.name = [CommonUtils checkEmptyObjects:[dict objectForKey:EVENT_NAME]];
        self.image = [CommonUtils checkEmptyObjects:[dict objectForKey:EVENT_IMAGE]];
        self.desc = [CommonUtils checkEmptyObjects:[dict objectForKey:EVENT_DESC]];
        self.date = [[CommonUtils checkEmptyObjects:[[dict objectForKey:EVENT_DATE] objectForKey:EVENT_DATE]] substringToIndex:19];
        self.address = [CommonUtils checkEmptyObjects:[dict objectForKey:EVENT_ADDRESS]];
        self.lat = [CommonUtils checkEmptyObjects:[dict objectForKey:EVENT_LAT]];
        self.lon = [CommonUtils checkEmptyObjects:[dict objectForKey:EVENT_LON]];
    }
    
    return self;
}


#pragma mark - UD

-(NSMutableArray*)getSavedDataForKey:(NSString*)key
{
    NSMutableArray *favs = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:key]];
    
    if (!favs) {
        favs = [[NSMutableArray alloc] init];
    }
    
    return favs;
}

-(void)setData:(NSMutableArray*)array forKey:(NSString*)key
{
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)addEventToSavedData:(NSString*)key
{
    NSMutableArray *array = [self getSavedDataForKey:key];
    
    if (![array containsObject:self.id_event]) {
        [array addObject:self.id_event];
    }
    
    [self setData:array forKey:key];
}

-(void)removeEventFromSavedData:(NSString*)key
{
    NSMutableArray *favs = [self getSavedDataForKey:key];
    
    if ([favs containsObject:self.id_event]) {
        [favs removeObject:self.id_event];
    }
    
    [self setData:favs forKey:key];
}


#pragma mark - Favs

-(void)addFavoutite
{
    [self addEventToSavedData:UD_FAVS];
}

-(void)removeFromFavourites
{
    [self removeEventFromSavedData:UD_FAVS];
}

-(BOOL)isFav
{
    return [[self getSavedDataForKey:UD_FAVS] containsObject:self.id_event];
}


#pragma mark - Alerts

-(void)addAlert
{
    [self addEventToSavedData:UD_ALERTS];
}

-(void)removeAlert
{
    [self removeEventFromSavedData:UD_ALERTS];
}

-(BOOL)hasAlert
{
    return [[self getSavedDataForKey:UD_ALERTS] containsObject:self.id_event];
}

@end
