//
//  MapUtils.m
//  SoundFermin
//
//  Created by Itziar on 5/6/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import "MapUtils.h"
#import "MainViewController.h"
#import "RequestUtilsEventsMap.h"
#import "Event.h"
#import "MyAnnotation.h"

@interface MapUtils ()
@property(nonatomic, strong) MainViewController *delegate;
@end

@implementation MapUtils

-(id)initWithDelegate:(UIViewController*)delegate
{
    self = [super init];
    
    if (self) {
        self.delegate = (MainViewController*)delegate;
    }
    
    return self;
}


#pragma mark - GET DATA

-(void)getData
{
    [super getData];
    
    RequestUtilsEventsMap *requestUtils = [[RequestUtilsEventsMap alloc] initWithDelegate:self];
    [requestUtils getRequest];
}

-(void)getDataSucceed:(NSMutableDictionary *)data
{
    [self.delegate.events removeAllObjects];
    
    for (NSDictionary *dict in [data objectForKey:kCONTENT_EVENTS]) {
        Event *event = [[Event alloc] initWithDictionary:dict];
        
        if (event) {
            [self.delegate.events addObject:event];
        }
    }
    
    [self.delegate loadAnnotations];
}

@end
