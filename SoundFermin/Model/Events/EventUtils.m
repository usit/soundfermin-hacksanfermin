//
//  EventUtils.m
//  SoundFermin
//
//  Created by Usue on 5/6/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import "EventUtils.h"
#import "EventsViewController.h"
#import "RequestUtilsEventsList.h"
#import "Event.h"

@interface EventUtils()

@property(nonatomic, strong) EventsViewController *delegate;

@end

@implementation EventUtils

-(id)initWithDelegate:(UIViewController*)delegate
{
    self = [super init];
    
    if (self) {
        self.delegate = (EventsViewController*)delegate;
    }
    
    return self;
}


#pragma mark - GET DATA

-(void)getData
{
    [super getData];
    
    int lastDownload = [[[NSUserDefaults standardUserDefaults] objectForKey:kUD_LAST_EVENTS_DOWNLOAD] intValue];
    
    if (([[NSDate date] timeIntervalSince1970] - lastDownload) > DAY_INTERVAL) {
        RequestUtilsEventsList *requestUtils = [[RequestUtilsEventsList alloc] initWithDelegate:self];
        [requestUtils getRequest];
    } else {
        [self getDataSucceed:[[NSUserDefaults standardUserDefaults] objectForKey:kUD_EVENTS_DATA]];
    }
}

-(void)getDataSucceed:(NSMutableDictionary *)data
{
    if ([[data objectForKey:kCONTENT_EVENTS] count] > 0) {
        [self.delegate.events removeAllObjects];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:kUD_EVENTS_DATA];
    }
    
    for (NSDictionary *dict in [data objectForKey:kCONTENT_EVENTS]) {
        Event *event = [[Event alloc] initWithDictionary:dict];
        NSString *day = [DateTimeUtils getDayFromDate:event.date];
        NSDate *date = [DateTimeUtils getDateFromString:event.date];
        NSDateComponents *dateComps = [[NSCalendar currentCalendar] components:NSCalendarUnitHour fromDate:date];
        
        if (dateComps && (dateComps.hour <= 3)) {
            day = [NSString stringWithFormat:@"%i", (day.intValue - 1)];
        }
        
        if (event) {
            NSMutableArray *dayArray;
            
            if ([self.delegate.events objectForKey:day]) {
                dayArray = [self.delegate.events objectForKey:day];
            } else {
                dayArray = [[NSMutableArray alloc] init];
            }
            
            [dayArray addObject:event];
            [self.delegate.events setObject:dayArray forKey:day];
        }
    }
    
    [self.delegate setTitleForSelectedDay];
    [self.delegate.tableEvents reloadData];
}

@end
