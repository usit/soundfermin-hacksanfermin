//
//  Event.h
//  SoundFermin
//
//  Created by Usue on 4/6/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Event : NSObject

#define EVENT_ID        @"id"
#define EVENT_NAME      @"name"
#define EVENT_IMAGE     @"image"
#define EVENT_DESC      @"description"
#define EVENT_DATE      @"date"
#define EVENT_ADDRESS   @"address"
#define EVENT_LAT       @"lat"
#define EVENT_LON       @"lon"

@property(nonatomic, strong) NSNumber *id_event;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *image;
@property(nonatomic, strong) NSString *desc;
@property(nonatomic, strong) NSString *date;
@property(nonatomic, strong) NSString *address;
@property(nonatomic, strong) NSNumber *lat;
@property(nonatomic, strong) NSNumber *lon;

-(id)initWithDictionary:(NSDictionary*)dict;

#pragma mark - Favs
-(void)addFavoutite;
-(void)removeFromFavourites;
-(BOOL)isFav;

#pragma mark - Alerts
-(void)addAlert;
-(void)removeAlert;
-(BOOL)hasAlert;

@end
