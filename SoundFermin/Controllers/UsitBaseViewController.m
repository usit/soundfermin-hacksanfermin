//
//  UsitBaseViewController.m
//
//  Created by Usue on 3/6/15.
//  Copyright (c) 2015 Usit Development. All rights reserved.
//

#import "UsitBaseViewController.h"

@interface UsitBaseViewController ()

@end

@implementation UsitBaseViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setLanguageTexts) name:@"languageChanged" object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setLanguageTexts];
    [self configureStatusAndNavBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)setLanguageTexts {}

-(void)configureStatusAndNavBar
{
    [self.navigationController.navigationBar setHidden:NO];
    [self setNavBarTitle];
    if (self.navigationController.viewControllers.count < 3) {
        [ViewCommonUtils setLeftButtonImage:@"menu_icon" withSelector:@selector(handleMenu) inTarget:self];
    } else {
        [ViewCommonUtils setBackButton:self];
    }
}

-(void)setNavBarTitle
{
    [ViewCommonUtils setNavBarTitle:@"nav_label_title" toController:self];
}


#pragma mark - Menu Helpers

-(void)handleMenu
{
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIF_HANDLE_MENU object:nil];
}

@end
