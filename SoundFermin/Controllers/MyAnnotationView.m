//
//  MyAnnotationView.m
//  SoundFermin
//
//  Created by Itziar on 4/6/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import "MyAnnotationView.h"
#import "MyAnnotation.h"

@implementation MyAnnotationView

-(id)initWithAnnotation:(id<MKAnnotation>)annotation
{
    self = [super initWithAnnotation:annotation reuseIdentifier:PIN_REUSE_ID];
    
    if (self) {
        MyAnnotation *myAnnotation = (MyAnnotation*)annotation;
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 80, 80);
        
        UIImageView *bg = [[UIImageView alloc] initWithFrame:self.frame];
        [bg setContentMode:UIViewContentModeScaleToFill];
        //[bg setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"boleteRojo" ofType:PNG]]];
        [bg setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:myAnnotation.image]]]];
        [bg setClipsToBounds:YES];
        [bg setAlpha:0.8];
        [ViewCommonUtils setCircleView:bg];
        [self addSubview:bg];
        
        UILabel *title = [[UILabel alloc] initWithFrame:self.frame];
        [title setTextColor:[UIColor whiteColor]];
        [title setTextAlignment:NSTextAlignmentCenter];
        [title setFont:[UIFont fontWithName:@"MarkerFelt-Thin" size:12.0]];
        [title setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.4]];
        [ViewCommonUtils setCircleView:title];
        
        NSString *when = [DateTimeUtils calculateTimeToDateEvent:myAnnotation.date];
        NSString *titleText = [NSString stringWithFormat:@"%@\n%@", myAnnotation.title, when];
        
        [title setText:titleText];
        [title setNumberOfLines:4];
        
        [self addSubview:title];
        [[self layer] setCornerRadius:self.frame.size.height/2];
    }
    
    return self;
}

@end
