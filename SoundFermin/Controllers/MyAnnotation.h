//
//  MyAnnotation.h
//  SoundFermin
//
//  Created by Itziar on 4/6/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MyAnnotation : MKPointAnnotation

@property(nonatomic) CLLocationCoordinate2D coordinate;
@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *subtitle;
@property(nonatomic, strong) NSString *image;
@property(nonatomic, strong) NSString *date;

-(id)initWithCoordinates:(CLLocationCoordinate2D)location name:(NSString*)name address:(NSString*)address image:(NSString*)image andDate:(NSString*)date;

@end
