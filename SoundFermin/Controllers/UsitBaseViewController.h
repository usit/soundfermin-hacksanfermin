//
//  UsitBaseViewController.h
//
//  Created by Usue on 3/6/15.
//  Copyright (c) 2015 Usit Development. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UsitBaseViewController : UIViewController

-(void)setLanguageTexts;
-(void)configureStatusAndNavBar;
-(void)setNavBarTitle;

#pragma mark - Menu Helpers
-(void)handleMenu;

@end
