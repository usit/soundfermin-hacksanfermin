//
//  MainViewController.h
//  SoundFermin
//
//  Created by Usue on 3/6/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import "UsitBaseViewController.h"
#import <MapKit/MapKit.h>

@interface MainViewController : UsitBaseViewController <MKMapViewDelegate, CLLocationManagerDelegate>

@property(nonatomic, strong) NSMutableArray *events;
@property(nonatomic, strong) IBOutlet MKMapView *mapView;
@property(nonatomic, strong) CLLocationManager *locationManager;
@property(nonatomic, strong) IBOutlet UIView *viewBGSwitch;
@property(nonatomic, strong) IBOutlet UISwitch *switchFav;
@property(nonatomic, strong) IBOutlet UILabel *labelFav;

-(void)closeMenu;
-(void)openMenu;

-(void)loadAnnotations;

@end
