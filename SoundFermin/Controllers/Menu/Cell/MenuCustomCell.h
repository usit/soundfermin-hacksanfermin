//
//  MenuCustomCell.h
//
//  Created by Usue on 3/6/15.
//  Copyright (c) 2015 Usit Development. All rights reserved.
//

@interface MenuCustomCell : UITableViewCell

-(void)setContentForData:(NSString*)title;

@end
