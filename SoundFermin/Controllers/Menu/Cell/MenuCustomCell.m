//
//  MenuCustomCell.m
//
//  Created by Usue on 3/6/15.
//  Copyright (c) 2015 Usit Development. All rights reserved.
//

#import "MenuCustomCell.h"

@interface MenuCustomCell()

@property(nonatomic, strong) IBOutlet UILabel *title;
@property(nonatomic, strong) IBOutlet UIImageView *imageMenu;

@end

@implementation MenuCustomCell

-(void)setContentForData:(NSString *)title
{
    [self.title setText:[CommonUtils localizedStrFor:title]];
    [self setSelectionStyle:UITableViewCellSelectionStyleGray];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
}

@end
