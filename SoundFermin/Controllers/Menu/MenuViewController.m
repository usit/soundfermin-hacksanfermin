//
//  MenuViewController.m
//
//  Created by Usue on 3/6/15.
//  Copyright (c) 2015 Usit Development. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuCustomCell.h"

#define arrayNotifs @[NOTIF_SHOW_MAP, NOTIF_SHOW_EVENTS, NOTIF_SHOW_ABOUT]
#define arrayTitles @[@"menu_label_map", @"menu_label_events", @"menu_label_about"]

@interface MenuViewController ()

@property(nonatomic, strong) IBOutlet UITableView *menu;

@end

@implementation MenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated
{
    CGRect frame = self.menu.frame;
    frame.size.height = [self.menu numberOfRowsInSection:0]*self.menu.rowHeight;
    [self.menu setFrame:frame];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayNotifs.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellId = @"MenuCustomCell";
    MenuCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:cellId owner:self options:nil] objectAtIndex:0];
    }
    
    [cell setContentForData:[arrayTitles objectAtIndex:indexPath.row]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSNotificationCenter defaultCenter] postNotificationName:[arrayNotifs objectAtIndex:indexPath.row] object:nil];
}

@end
