//
//  MyAnnotationView.h
//  SoundFermin
//
//  Created by Itziar on 4/6/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "Event.h"

#define PIN_REUSE_ID @"SFPinAnnotationView"

@interface MyAnnotationView : MKAnnotationView

-(id)initWithAnnotation:(id<MKAnnotation>)annotation;

@end
