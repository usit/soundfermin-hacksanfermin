//
//  EventCustomCell.h
//
//  Created by Usue on 4/6/15.
//  Copyright (c) 2015 Usit Development. All rights reserved.
//

#import "Event.h"
#import "EventsViewController.h"

@interface EventCustomCell : UITableViewCell <UIAlertViewDelegate>

-(void)setContentForData:(Event*)event delegate:(EventsViewController*)delegate;

@end
