//
//  EventCustomCell.m
//
//  Created by Usue on 4/6/15.
//  Copyright (c) 2015 Usit Development. All rights reserved.
//

#import "EventCustomCell.h"

#define ALERT_ID @"alertId"

@interface EventCustomCell()

@property(nonatomic, strong) Event *currentEvent;
@property(nonatomic, strong) IBOutlet UILabel *title;
@property(nonatomic, strong) IBOutlet UILabel *address;
@property(nonatomic, strong) IBOutlet UILabel *date;
@property(nonatomic, strong) IBOutlet UILabel *time;
@property(nonatomic, strong) IBOutlet UIButton *fav;
@property(nonatomic, strong) IBOutlet UIButton *alert;
@property(nonatomic, strong) IBOutlet UIImageView *image;
@property(nonatomic, strong) IBOutlet UIActivityIndicatorView *actView;
@property(nonatomic, strong) EventsViewController *delegate;

@end

@implementation EventCustomCell

-(void)setContentForData:(Event*)event delegate:(EventsViewController *)delegate
{
    self.currentEvent = event;
    self.delegate = delegate;
    
    [self.title setText:event.name];
    [self.address setText:event.address];
    [self.date setText:[DateTimeUtils getDate:event.date]];
    [self.time setText:[DateTimeUtils getTime:event.date]];
    [self.image setImage:nil];
    [self.fav setSelected:[event isFav]];
    
    [[[LazyLoadingUtils alloc] init] setViewImage:event.image :kUD_CACHE_USERS :self.image :self.actView];
    
    [self setSelectionStyle:UITableViewCellSelectionStyleGray];
}

#pragma mark - Actions

-(IBAction)pressFav:(id)sender
{
    if ([sender isSelected]) {
        [self.currentEvent removeFromFavourites];
    } else {
        [self.currentEvent addFavoutite];
    }
    
    [sender setSelected:![sender isSelected]];
}

-(IBAction)pressAlert:(id)sender
{
    if ([sender isSelected]) {
        [self confirmDeleteAlert];
    } else {
        [self confirmAlert];
    }
    
    [sender setSelected:![sender isSelected]];
}

-(void)confirmAlert
{
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    NSDate *date = [DateTimeUtils getDateFromString:self.currentEvent.date];
    date = [date dateByAddingTimeInterval:(-3600)];
    
    notification.fireDate = date;
    notification.alertTitle = [CommonUtils localizedStrFor:@"alert_event_title"];
    notification.alertBody = [NSString stringWithFormat:@"%@ - %@ %@", self.currentEvent.name, [DateTimeUtils getDate:self.currentEvent.date], [DateTimeUtils getTime:self.currentEvent.date]];
    notification.hasAction = NO;
    notification.soundName = UILocalNotificationDefaultSoundName;
    notification.userInfo = @{ALERT_ID: self.currentEvent.id_event};
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    
    [self.currentEvent addAlert];
    [self shareOnMedia];
}

-(void)confirmDeleteAlert
{
    NSArray *eventArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
    
    for (int i=0; i<[eventArray count]; i++) {
        UILocalNotification* oneEvent = [eventArray objectAtIndex:i];
        NSDictionary *userInfoCurrent = oneEvent.userInfo;
        NSNumber *alertId = [userInfoCurrent valueForKey:ALERT_ID];

        if ([alertId isEqualToNumber:self.currentEvent.id_event]) {
            [[UIApplication sharedApplication] cancelLocalNotification:oneEvent];
            break;
        }
    }
    
    [self.currentEvent removeAlert];
}

-(void)shareOnMedia
{
    [[ViewCommonUtils sharedInstance] showTwoButtonsAlert:self
                                                    title:@"Sound Fermin"
                                                      msg:@"share_title"
                                             buttonCancel:@"cancel"
                                              otherButton:@"accept"];
}

-(void)confirmShareOnMedia
{
    self.delegate.shareUtils = [[ShareUtils alloc] initWithDelegate:self.delegate
                                                              event:self.currentEvent];
    [self.delegate.shareUtils openShareController];
}


#pragma mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        if ([alertView.message isEqualToString:[CommonUtils localizedStrFor:@"share_title"]]) {
            [self confirmShareOnMedia];
        }
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
}

@end
