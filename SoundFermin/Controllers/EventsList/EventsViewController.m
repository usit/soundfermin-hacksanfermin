//
//  EventsViewController.m
//  SoundFermin
//
//  Created by Usue on 4/6/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import "EventsViewController.h"
#import "EventCustomCell.h"
#import "EventUtils.h"
#import "EventDetailViewController.h"

@interface EventsViewController ()

@property(nonatomic, strong) NSString *selectedDay;
@property(nonatomic, strong) EventUtils *dataUtils;
@property(nonatomic, strong) IBOutletCollection(UIButton) NSArray *days;

@end

@implementation EventsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.selectedDay = @"6";
    self.events = [[NSMutableDictionary alloc] init];
    self.dataUtils = [[EventUtils alloc] initWithDelegate:self];
    
    for (UIButton *day in self.days) {
        [day addTarget:self action:@selector(selectDay:) forControlEvents:UIControlEventTouchUpInside];
        [ViewCommonUtils setCircleView:day];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.dataUtils getData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)selectDay:(UIButton*)sender
{
    self.selectedDay = [NSString stringWithFormat:@"%i", (int)sender.tag];
    [self.tableEvents reloadData];
    
    [self setTitleForSelectedDay];
}

-(void)setTitleForSelectedDay
{
    NSArray *eventsDay = [self.events objectForKey:self.selectedDay];
    
    if (eventsDay.count > 0) {
        Event *event = [eventsDay firstObject];
        [ViewCommonUtils setNavBarTitle:[DateTimeUtils convertServerDate:event.date] toController:self];
    }
}


#pragma mark - UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.events objectForKey:self.selectedDay] count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellId = @"EventCustomCell";
    EventCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:cellId owner:self options:nil] objectAtIndex:0];
    }
    
    [cell setContentForData:[[self.events objectForKey:self.selectedDay] objectAtIndex:indexPath.row] delegate:self];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventDetailViewController *eventDetailVC = [[EventDetailViewController alloc] initWithNibName:nil bundle:nil];
    [eventDetailVC setEvent:[[self.events objectForKey:self.selectedDay] objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:eventDetailVC animated:YES];
}

@end
