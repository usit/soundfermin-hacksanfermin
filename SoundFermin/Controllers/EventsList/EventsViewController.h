//
//  EventsViewController.h
//  SoundFermin
//
//  Created by Usue on 4/6/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import "UsitBaseViewController.h"
#import "ShareUtils.h"

@interface EventsViewController : UsitBaseViewController <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) ShareUtils *shareUtils;
@property(nonatomic, strong) NSMutableDictionary *events;
@property(nonatomic, strong) IBOutlet UITableView *tableEvents;

-(void)setTitleForSelectedDay;

@end
