//
//  EventDetailViewController.h
//  SoundFermin
//
//  Created by Itziar on 11/7/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import <MapKit/MapKit.h>

@interface EventDetailViewController : UsitBaseViewController <MKMapViewDelegate>

@property(nonatomic, strong) Event *event;

@end
