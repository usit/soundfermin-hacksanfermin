//
//  EventDetailViewController.m
//  SoundFermin
//
//  Created by Itziar on 11/7/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import "EventDetailViewController.h"
#import "MyAnnotationView.h"
#import "MyAnnotation.h"

@interface EventDetailViewController ()
@property(nonatomic, strong) IBOutlet UIImageView *imageViewEvent;
@property(nonatomic, strong) IBOutlet UILabel *labelName;
@property(nonatomic, strong) IBOutlet UILabel *labelPlace;
@property(nonatomic, strong) IBOutlet UILabel *labelDateTime;
@property(nonatomic, strong) IBOutlet UILabel *labelDescription;
@property(nonatomic, strong) IBOutlet UILabel *labelGetThere;
@property(nonatomic, strong) IBOutlet MKMapView *mapView;
@end

@implementation EventDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self configureView];
}

-(void)configureView
{
    [self.imageViewEvent setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.event.image]]]];
    [self.labelName setText:self.event.name];
    [self.labelPlace setText:self.event.address];
    [self.labelDateTime setText:[NSString stringWithFormat:@"%@ - %@", [DateTimeUtils getDate:self.event.date], [DateTimeUtils getTime:self.event.date]]];
    [self.labelDescription setText:self.event.desc];
    [self.labelGetThere setText:[CommonUtils localizedStrFor:@"eventDetail_label_getThere"]];
    
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(self.event.lat.floatValue, self.event.lon.floatValue);
    MyAnnotation *annotation = [[MyAnnotation alloc] initWithCoordinates:coord name:self.event.name address:self.event.address image:self.event.image andDate:self.event.date];
    [self.mapView addAnnotation:annotation];
    
    [self centerAndSpanTheMap:coord];
}

-(void)centerAndSpanTheMap:(CLLocationCoordinate2D)coord
{
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    
    span.latitudeDelta = 0.00001;
    span.longitudeDelta = 0.00001;
    
    region.span = span;
    region.center = coord;
    
    [self.mapView setRegion:region animated:TRUE];
    [self.mapView regionThatFits:region];
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    if ([annotation isKindOfClass:[MKPointAnnotation class]]) {
        MyAnnotationView *pinView = (MyAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:PIN_REUSE_ID];
        
        if (!pinView) {
            pinView = [[MyAnnotationView alloc] initWithAnnotation:annotation];
            pinView.canShowCallout = YES;
        } else {
            pinView.annotation = annotation;
        }
        
        return pinView;
    }
    return nil;
}

-(void)setLanguageTexts
{
    
}

@end
