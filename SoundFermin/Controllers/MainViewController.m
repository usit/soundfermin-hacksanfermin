//
//  MainViewController.m
//  SoundFermin
//
//  Created by Usue on 3/6/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import "MainViewController.h"
#import "EventsViewController.h"
#import "MyAnnotationView.h"
#import "MyAnnotation.h"
#import "MapUtils.h"

@interface MainViewController ()

@property(nonatomic, strong) EventsViewController *eventsView;
@property(nonatomic, strong) MapUtils *dataUtils;
@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMenu) name:NOTIF_HANDLE_MENU object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showEventList) name:NOTIF_SHOW_EVENTS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMap) name:NOTIF_SHOW_MAP object:nil];
 
    self.events = [[NSMutableArray alloc] init];
    self.dataUtils = [[MapUtils alloc] initWithDelegate:self];
    [self initializeLocationManager];
    [self initializeSwitchToAllEvents];
    [self.viewBGSwitch.layer setCornerRadius:5];
}

-(void)initializeSwitchToAllEvents
{
    [self.switchFav setOn:NO];
    [self setLabelFav];
}

-(void)setLabelFav
{
    [self.labelFav setText:(self.switchFav.isOn)?[CommonUtils localizedStrFor:@"map_label_switch_on"]: [CommonUtils localizedStrFor:@"map_label_switch_off"]];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(!self.locationManager) [self initializeLocationManager];
    [self.locationManager startUpdatingLocation];
    
    [self.dataUtils getData];
}

-(void)loadAnnotations
{
    if(self.mapView.annotations.count>0){
        [self.mapView removeAnnotations:self.mapView.annotations];
    }
    CLLocationCoordinate2D coords[self.events.count];
    int i=0, j=0;
    for (Event *event in self.events) {
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(event.lat.floatValue, event.lon.floatValue);
        coords[i] = coord;
        MyAnnotation *annotation = [[MyAnnotation alloc] initWithCoordinates:coord name:event.name address:event.desc image:event.image andDate:event.date];
        if ((self.switchFav.isOn)&&([event isFav])){
            [self.mapView addAnnotation:annotation];
            j++;
        }else if (!self.switchFav.isOn){
            [self.mapView addAnnotation:annotation];
            i++;
        }
    }
    if ((j&&self.switchFav.isOn)||(i)) {
        [self.mapView setRegion:[CommonUtils coordinateRegionForCoordinates:coords coordCount:(self.switchFav.isOn)?j:i] animated:YES];
    }
}

-(void)initializeLocationManager
{
    self.locationManager = [[CLLocationManager alloc] init];
    [self.locationManager setDelegate:self];
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager requestAlwaysAuthorization];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Menu Helpers

-(void)handleMenu
{
    if ((self.navigationController.view.frame.origin.x==0)) {
        [self openMenu];
    } else {
        [self closeMenu];
    }
}

- (void)closeMenu
{
    if (self.navigationController.view.frame.origin.x != 0) {
        [self animateMenuToEndRectX:0 :@(10)];
    }
}

- (void)openMenu
{
    int flap = 44;
    [self animateMenuToEndRectX:self.navigationController.view.frame.size.width - flap :@(-10)];
}

- (void)animateMenuToEndRectX:(CGFloat)endRectX :(NSNumber *)to
{
    UIView *currentView = self.navigationController.view;
    CGRect screen = currentView.frame;
    CGRect endRect = CGRectMake(endRectX, screen.origin.y, screen.size.width, screen.size.height);
    
    [ViewCommonUtils commitAnimationFor:currentView :0.3 :endRect :UIViewAnimationCurveEaseInOut];
    [ViewCommonUtils performSelector:@selector(commitBounceAnimation:)
                          withObject:@[currentView, @(0.15), @(0), @(0), to]
                          afterDelay:0.3];
}

#pragma mark - Menu Actions

-(void)showMap
{
    [self closeMenu];
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}

-(void)showEventList
{
    [self closeMenu];
    
    [self.navigationController popToRootViewControllerAnimated:NO];
    self.eventsView = [[EventsViewController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:self.eventsView animated:YES];
}


#pragma mark - MKMap Delegate

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{}

-(void)mapViewDidFinishLoadingMap:(MKMapView *)mapView
{
}

-(void)mapViewDidStopLocatingUser:(MKMapView *)mapView{}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"%@", [locations lastObject]);
//    CLLocation *newLocation = locations.lastObject;
//    
//    NSTimeInterval locationAge = -[newLocation.timestamp timeIntervalSinceNow];
//    if (locationAge > 5.0) return;
//    
//    NSLog(@"horizontal accuracy %f", newLocation.horizontalAccuracy);
//    
//    if (newLocation.horizontalAccuracy < 0) return;
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    if ([annotation isKindOfClass:[MKPointAnnotation class]]) {
        MyAnnotationView *pinView = (MyAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:PIN_REUSE_ID];
        
        if (!pinView) {
            pinView = [[MyAnnotationView alloc] initWithAnnotation:annotation];
            pinView.canShowCallout = YES;
        } else {
            pinView.annotation = annotation;
        }
        
        return pinView;
    }
    return nil;
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    
}

#pragma mark - Switch

-(IBAction)pressSwitch:(id)sender
{
    [self setLabelFav];
    [self loadAnnotations];
}

@end
