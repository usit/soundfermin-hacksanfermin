//
//  MyAnnotation.m
//  SoundFermin
//
//  Created by Itziar on 4/6/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import "MyAnnotation.h"

@implementation MyAnnotation

-(id)initWithCoordinates:(CLLocationCoordinate2D)location name:(NSString*)name address:(NSString*)address image:(NSString*)image andDate:(NSString*)date
{
    self = [super init];
    
    if (self) {
        self.title = name;
        self.subtitle = address;
        self.coordinate = location;
        self.image = image;
        self.date = date;
    }
    
    return self;
}

@end
