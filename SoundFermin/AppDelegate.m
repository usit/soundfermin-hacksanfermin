//
//  AppDelegate.m
//  SoundFermin
//
//  Created by Usue on 3/6/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window setBackgroundColor:[UIColor lightGrayColor]];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    self.mainView = [[MainViewController alloc] initWithNibName:nil bundle:nil];
    self.navController = [[UINavigationController alloc] initWithRootViewController:self.mainView];
    self.menuView = [[MenuViewController alloc] initWithNibName:nil bundle:nil];
    [self.menuView.view setFrame:self.window.frame];
    
    //[self addGestureRecognizers];
    [self registerNotifications];
    
    [self.window setRootViewController:self.navController];
    [self.window addSubview:self.menuView.view];
    [self.window makeKeyAndVisible];
    
    return YES;
}

-(void)registerNotifications
{
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    }
}

-(void)addGestureRecognizers
{
    UISwipeGestureRecognizer *swipeGestureLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(closeMenu)];
    [swipeGestureLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    UISwipeGestureRecognizer *swipeGestureRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(openMenu)];
    [swipeGestureRight setDirection:UISwipeGestureRecognizerDirectionRight];
    
    [self.navController.view addGestureRecognizer:swipeGestureLeft];
    [self.navController.view addGestureRecognizer:swipeGestureRight];
    
    if ([self.navController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navController.interactivePopGestureRecognizer.enabled = NO;
    }
}

-(void)closeMenu
{
    if ([self menuIsOpen]) {
        [self.mainView closeMenu];
    }
}

-(void)openMenu
{
    if (![self menuIsOpen]) {
        [self.mainView openMenu];
    }
}

-(BOOL)menuIsOpen
{
    return (self.navController.view.frame.origin.x!=0);
}


#pragma mark - Notifications

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    [[ViewCommonUtils sharedInstance] showOneButtonAlert:self title:notification.alertTitle msg:notification.alertBody buttonTitle:@"accept"];
}


#pragma mark - App Life

- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}

@end
