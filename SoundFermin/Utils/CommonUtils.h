//
//  CommonUtils.h
//
//  Created by Usue on 3/6/15.
//  Copyright (c) 2015 Usit Development. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface CommonUtils : NSObject

#pragma mark - Common
+(NSString*)getDeviceLanguage;
+(void)setDeviceLanguage:(NSString*)language;
+(NSString*)localizedStrFor:(NSString*)str;
+(id)checkEmptyObjects:(id)text;
+(BOOL)isEmpty:(id)text;
+(void)addObjectUnique:(id)object inArray:(NSMutableArray*)array;
+(MKCoordinateRegion)coordinateRegionForCoordinates:(CLLocationCoordinate2D*)coords coordCount:(int)coordCount;

@end
