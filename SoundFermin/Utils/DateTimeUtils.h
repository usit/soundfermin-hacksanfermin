//
//  DateTimeUtils.h
//  SoundFermin
//
//  Created by Itziar on 5/6/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import "BaseUtils.h"

@interface DateTimeUtils : BaseUtils

#pragma mark - Date

+(NSString*)convertServerDate:(NSString*)date;
+(NSString*)getDayFromDate:(NSString*)date;
+(NSString*)calculateTimeToDateEvent:(NSString*)date;
+(NSDateComponents*)differenceBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
+(NSString*)getStringFromDate:(NSDate*)date;
+(NSDate*)getDateFromString:(NSString*)date;
+(NSString*)getDate:(NSString*)date;
+(NSString*)getTime:(NSString*)date;

@end
