//
//  UIColor+ColorWithComponents.m
//
//  Created by Usue on 3/6/15.
//  Copyright (c) 2015 Usit Development. All rights reserved.
//

#import "UIColor+ColorWithComponents.h"

@implementation UIColor (ColorWithComponents)

+(UIColor*)colorWithHexRed:(int)red green:(int)green blue:(int)blue alpha:(int)alpha
{
    return [UIColor colorWithRed:red/(float)0xFF green:green/(float)0xFF blue:blue/(float)0xFF alpha:alpha/(float)0xFF];
}

+(uint)componentFrom:(uint)c offset:(short)o
{
    short componentSize = 8;
    return (c >> (o*componentSize)) & 0x000000FF;
}

+(UIColor*)colorWithARGBInt:(uint)color
{
    short pos = 3;
    uint a = [UIColor componentFrom:color offset:pos--];
    uint r = [UIColor componentFrom:color offset:pos--];
    uint g = [UIColor componentFrom:color offset:pos--];
    uint b = [UIColor componentFrom:color offset:pos--];
    return [UIColor colorWithHexRed:r green:g blue:b alpha:a];
}

@end
