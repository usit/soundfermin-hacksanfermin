//
//  AlertManager.h
//
//  Created by Usue Napal on 20/05/15
//  Copyright (c) 2013 Usit Development All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlertManager : NSObject <UIAlertViewDelegate>

@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *msg;

-(void)showOneButtonAlert:(id)target;
-(void)showTwoButtonsAlert:(id)target;
-(BOOL)checkSuccess:(NSMutableDictionary*)dictionaryResponse inSilence:(BOOL)silence;

@end
