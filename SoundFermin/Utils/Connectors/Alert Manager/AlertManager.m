//
//  AlertManager.m
//
//  Created by Usue Napal on 20/05/15
//  Copyright (c) 2013 Usit Development All rights reserved.
//

#import "AlertManager.h"
#import "AppDelegate.h"

@implementation AlertManager

@synthesize title, msg;
    
-(void)showOneButtonAlert:(id)target
{
    [[ViewCommonUtils sharedInstance] showOneButtonAlert:target title:title msg:msg buttonTitle:@"accept"];
}

-(void)showTwoButtonsAlert:(id)target
{
    [[ViewCommonUtils sharedInstance] showTwoButtonsAlert:target title:title msg:msg buttonCancel:@"cancel" otherButton:@"accept"];
}

-(BOOL)checkSuccess:(NSMutableDictionary*)dictionaryResponse inSilence:(BOOL)silence
{
    BOOL success = [[dictionaryResponse objectForKey:kSTATUS] isEqualToString:kSUCCESS];
    
    if (!success) {
        [self handleFormErrors:dictionaryResponse silence:silence];
    }
    
    return success;
}

#pragma mark - Form errors

-(void)handleFormErrors:(NSMutableDictionary*)dictContent silence:(BOOL)silence
{
    /*if ((!silence) && ([dictContent objectForKey:kMESSAGE])) {
        int code = [[dictContent objectForKey:kCODE] intValue];
            
        if (!((code==kCODE_INVALID_TOKEN) || (code==kCODE_INVALID_TOKEN_OR_NO_USER))) {
            [self handleSpecificErrors:[dictContent objectForKey:kMESSAGE]];
        } else {
            [[[UserUtils alloc] initWithDelegate:nil] getData];
        }
    }*/
}

-(void)handleSpecificErrors:(NSString*)message
{
    [self setTitle:@"alert_error_title_default"];
	[self setMsg:message];
	[self showOneButtonAlert:nil];
}

@end
