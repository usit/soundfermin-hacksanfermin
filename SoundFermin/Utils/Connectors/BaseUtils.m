//
//  BaseUtils.m
//
//  Created by Usue Napal on 20/05/15
//  Copyright (c) 2013 Usit Development All rights reserved.
//

#import "BaseUtils.h"

#define COLOR_TEXTFIELD_BG          [UIColor whiteColor]
#define COLOR_TEXTFIELD_BG_ERROR    [UIColor redColor]

@implementation BaseUtils

-(id)initWithDelegate:(UIViewController*)delegate
{
    self = [super init];
    if (self) {
        self.delegate = delegate;
    }
    return self;
}

-(void)getData
{
    [self.delegate.view endEditing:YES];
    //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
}

-(void)getDataSucceed:(NSMutableDictionary*)data
{
    /*if ([[UIApplication sharedApplication] isIgnoringInteractionEvents]) {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }*/
}

-(void)postData
{
    [self.delegate.view endEditing:YES];
    //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
}

-(void)postedDataSucceed:(NSMutableDictionary*)data
{
    /*if ([[UIApplication sharedApplication] isIgnoringInteractionEvents]) {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }*/
}

-(void)connectionError
{
    [self.refreshControl endRefreshing];
}

-(void)addRefreshControlToTable:(UITableView*)table
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl setTintColor:[UIColor darkGrayColor]];
    [table addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(getData) forControlEvents:UIControlEventValueChanged];
}


#pragma mark - Helpers

-(BOOL)requiredFieldsAreEmpty
{
    BOOL requiredFieldsEmpty = NO;
    
    for (UITextField *textField in self.requiredFields) {
        if ((!textField.text) || ([textField.text isEqualToString:@""])) {
            requiredFieldsEmpty = YES;
            [self setColorForInvalidField:textField];
        } else {
            [self setColorForValidField:textField];
        }
    }
    
    return requiredFieldsEmpty;
}

-(void)setColorForValidField:(UIView*)view
{
    [view setBackgroundColor:COLOR_TEXTFIELD_BG];
}

-(void)setColorForInvalidField:(UIView*)view
{
    [view setBackgroundColor:COLOR_TEXTFIELD_BG_ERROR];
}

@end
