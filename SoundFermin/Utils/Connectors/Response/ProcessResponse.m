//
//  ProcessResponse.m
//
//  Created by Usit Development on 6/11/13.
//  Copyright (c) 2013 Usit Development All rights reserved.
//

#import "ProcessResponse.h"

@interface ProcessResponse()

@property(nonatomic) BOOL showConnectionError;

@end

@implementation ProcessResponse

@synthesize receiveData, dictionaryResponse;

-(id)initWithConnectionError:(BOOL)showError delegate:(id)delegate
{
    self = [super init];
    if (self) {
        alertManager = [[AlertManager alloc] init];
        self.showConnectionError = showError;
        self.delegate = delegate;
    }
    return self;
}

-(void)endIgnoringEvents
{
    if ([[UIApplication sharedApplication] isIgnoringInteractionEvents])
		[[UIApplication sharedApplication] endIgnoringInteractionEvents];
}

-(void)endIgnoringEventsAndRemoveSpinner
{
    [self endIgnoringEvents];
    [[ViewCommonUtils sharedInstance] performSelectorInBackground:@selector(removeSpinner) withObject:nil];
}

#pragma mark - NSURLConnectionDelegate

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	if (self.showConnectionError) {
        [[ViewCommonUtils sharedInstance] showOneButtonAlert:nil
                                                       title:@"alert_connection_title"
                                                         msg:@"alert_connection_message"
                                                 buttonTitle:@"accept"];
    }
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"connectionError" object:nil];
	[self endIgnoringEvents];
    [[ViewCommonUtils sharedInstance] performSelectorInBackground:@selector(removeSpinner) withObject:nil];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (!receiveData) {
        receiveData = [NSMutableData data];
    }
    
    [receiveData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error;
    dictionaryResponse = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:receiveData options:0 error:&error]];
    [self processResponse];
}

-(void)processResponse {}

@end
