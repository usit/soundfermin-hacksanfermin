//
//  ProcessResponse.h
//
//  Created by Usue Napal on 20/05/15
//  Copyright (c) 2013 Usit Development All rights reserved.
//

#import "AlertManager.h"

@protocol ProccessResponseProtocol <NSObject>

@required
-(void)processResponse;

@end


@interface ProcessResponse : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate, ProccessResponseProtocol>
{
    NSMutableData *receiveData;
    NSMutableDictionary *headers;
    NSMutableDictionary *dictionaryResponse;
    AlertManager *alertManager;
}

@property(nonatomic, strong) id delegate;
@property(nonatomic, strong) NSMutableData *receiveData;
@property(nonatomic, strong) NSMutableDictionary *dictionaryResponse;

-(id)initWithConnectionError:(BOOL)showError delegate:(id)delegate;
-(void)endIgnoringEvents;
-(void)endIgnoringEventsAndRemoveSpinner;

@end
