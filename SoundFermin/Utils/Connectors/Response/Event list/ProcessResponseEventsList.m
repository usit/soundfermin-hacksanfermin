//
//  ProcessResponseEventsList.m
//  SoundFermin
//
//  Created by Usue on 5/6/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import "ProcessResponseEventsList.h"
#import "EventUtils.h"

@implementation ProcessResponseEventsList

-(void)processResponse
{
    if ([alertManager checkSuccess:dictionaryResponse inSilence:NO]) {
        [self.delegate getDataSucceed:dictionaryResponse];
        [[NSUserDefaults standardUserDefaults] setObject:@([[NSDate date] timeIntervalSince1970]) forKey:kUD_LAST_EVENTS_DOWNLOAD];
    }
    
    [self endIgnoringEventsAndRemoveSpinner];
}

@end
