//
//  ProcessResponseEventsMap.m
//  SoundFermin
//
//  Created by Itziar on 5/6/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import "ProcessResponseEventsMap.h"
#import "MapUtils.h"

@implementation ProcessResponseEventsMap

-(void)processResponse
{
    if ([alertManager checkSuccess:dictionaryResponse inSilence:NO]) {
        [self.delegate getDataSucceed:dictionaryResponse];
    }
    
    [self endIgnoringEventsAndRemoveSpinner];
}

@end
