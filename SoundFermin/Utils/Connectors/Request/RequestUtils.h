//
//  RequestUtils.h
//
//  Created by Usue Napal on 20/05/15
//  Copyright (c) 2013 Usit Development All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProcessResponse.h"
#import "BaseUtils.h"

#define GET_METHOD @"GET"
#define POST_METHOD @"POST"
#define PUT_METHOD @"PUT"
#define DELETE_METHOD @"DELETE"

@interface RequestUtils : NSObject

@property(nonatomic, strong) NSMutableArray *arrayData;
@property(nonatomic, strong) NSMutableDictionary *params;
@property(nonatomic, strong) id delegate;
@property(nonatomic, strong) ProcessResponse *processResponse;

-(id)initWithDelegate:(BaseUtils*)delegate;

-(NSString*)urlModule:(NSString *)module;
-(void)getRequestWithUrl:(NSString *)url auth:(BOOL)authenticate;
-(void)postRequestWithUrl:(NSString *)url auth:(BOOL)authenticate;
-(void)deleteRequestWithUrl:(NSString *)url auth:(BOOL)authenticate;
-(void)putRequestWithUrl:(NSString *)url auth:(BOOL)authenticate;
-(void)preparePostParameter:(NSString*)name :(id)value;

@end
