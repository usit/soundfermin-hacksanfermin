//
//  RequestUtilsEventsMap.m
//  SoundFermin
//
//  Created by Itziar on 5/6/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import "RequestUtilsEventsMap.h"
#import "ProcessResponseEventsMap.h"

@implementation RequestUtilsEventsMap

-(void)getRequest
{
    [[ViewCommonUtils sharedInstance] addSpinner];
    
    self.processResponse = [[ProcessResponseEventsMap alloc] initWithConnectionError:YES
                                                                             delegate:self.delegate];
    [self getRequestWithUrl:[self urlModule:kCURRENT_EVENTS_URL] auth:YES];
}

@end
