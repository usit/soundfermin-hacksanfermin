//
//  RequestUtilsEventsMap.h
//  SoundFermin
//
//  Created by Itziar on 5/6/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import "RequestUtils.h"

@interface RequestUtilsEventsMap : RequestUtils

-(void)getRequest;

@end
