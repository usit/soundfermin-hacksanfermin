//
//  RequestUtils.m
//
//  Created by Usue Napal on 20/05/15
//  Copyright (c) 2013 Usit Development All rights reserved.
//

#import "RequestUtils.h"

#define CONTENT_TYPE_JSON @"application/json;"
#define CONTENT_TYPE_FORM @"multipart/form-data;"
#define CONTENT_TYPE_ENCODED @"application/x-www-form-urlencoded"

#define PARAM_NAME  @"name"
#define PARAM_VALUE @"value"

@implementation RequestUtils

-(id)initWithDelegate:(BaseUtils*)delegate
{
    self = [super init];
    
    if (self) {
        self.delegate = delegate;
        self.params = [[NSMutableDictionary alloc] init];
        self.arrayData = [[NSMutableArray alloc] init];
    }
    return self;
}


#pragma mark - Public methods

-(NSString*)urlModule:(NSString *)module
{
    return [NSString stringWithFormat:@"%@%@", SERVER_BASE_URL, module];
}

-(void)getRequestWithUrl:(NSString *)url auth:(BOOL)authenticate
{
    [self requestForMethod:GET_METHOD url:url auth:authenticate];
}

-(void)postRequestWithUrl:(NSString *)url auth:(BOOL)authenticate
{
    [self requestForMethod:POST_METHOD url:url auth:authenticate];
}

-(void)deleteRequestWithUrl:(NSString *)url auth:(BOOL)authenticate
{
    [self requestForMethod:DELETE_METHOD url:url auth:authenticate];
}

-(void)putRequestWithUrl:(NSString *)url auth:(BOOL)authenticate
{
    [self requestForMethod:PUT_METHOD url:url auth:authenticate];
}


#pragma mark - Private methods

-(void)setAuthenticationHeader:(NSString*)url
{
    /*NSString *userId = [UserCommonUtils getUserId];
    NSString *token = [UserCommonUtils getUserToken];
    NSString *language = [CommonUtils getDeviceLanguage];
    
    return [url stringByAppendingFormat:@"?%@=%@&%@=%@&%@=%@&%@=%@&%@=%@",AUTH_USER, userId, AUTH_TOKEN, token, AUTH_LANGUAGE, language, AUTH_VERSION, VERSION, AUTH_PLATFORM, PLATFORM];*/
}

-(void)requestForMethod:(NSString*)method url:(NSString*)url auth:(BOOL)authenticate
{
    NSString *boundary = [NSString stringWithFormat:@"------%ld__%ld__%ld", random(), random(), random()];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:method];
    
    if (![method isEqualToString:GET_METHOD]) {
        [self setPostHeaderForRequest:request :boundary];
        [request setHTTPBody:[self setPostParametersBody:request :boundary]];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [NSURLConnection connectionWithRequest:request delegate:self.processResponse];
    });
}

-(void)preparePostParameter:(NSString*)name :(id)value
{
    //[self.params setObject:value forKey:name];
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:name, PARAM_NAME, value, PARAM_VALUE, nil];
    [self.arrayData addObject:dict];
}


#pragma mark - Private methods

-(void)setHeaderForRequest:(NSMutableURLRequest*)request boundary:(NSString*)boundary type:(NSString*)type
{
    NSString *contentType = [NSString stringWithFormat:@"%@ boundary=%@",type, boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
}

-(void)setPostHeaderForRequest:(NSMutableURLRequest*)request :(NSString*)boundary
{
    [self setHeaderForRequest:request boundary:boundary type:CONTENT_TYPE_FORM];
}

#pragma mark - Parameters Helpers

-(NSMutableData*)setPostParametersBody:(NSMutableURLRequest*)request :(NSString*)boundary
{
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
//    for (NSString *key in self.params) {
//        id value = [self.params objectForKey:key];
//        
//        if([value isKindOfClass:[UIImage class]]) {
//            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"pic.jpg\"\r\n\r\n",key] dataUsingEncoding:NSUTF8StringEncoding]];
//            [body appendData:[NSData dataWithData:value]];
//        } else {
//            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",key] dataUsingEncoding:NSUTF8StringEncoding]];
//            
//            if ([value isKindOfClass:[NSNumber class]]){
//                [body appendData:[[NSString stringWithFormat:@"%@",value] dataUsingEncoding:NSUTF8StringEncoding]];
//            }else if ([value isKindOfClass:[NSData class]]){
//                [body appendData:value];
//            }else{
//                [body appendData:[value dataUsingEncoding:NSUTF8StringEncoding]];
//            }
//        }
//        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    }
    
    for (id currentObject in self.arrayData) {
        NSString *name = [currentObject objectForKey:PARAM_NAME];
        id value = [currentObject objectForKey:PARAM_VALUE];
        
        if([value isKindOfClass:[UIImage class]]) {
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"pic.jpg\"\r\n\r\n",name] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:value]];
        } else {
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",name] dataUsingEncoding:NSUTF8StringEncoding]];
            
            if ([value isKindOfClass:[NSNumber class]]){
                [body appendData:[[NSString stringWithFormat:@"%@",value] dataUsingEncoding:NSUTF8StringEncoding]];
            }else if ([value isKindOfClass:[NSData class]]){
                [body appendData:value];
            }else{
                [body appendData:[value dataUsingEncoding:NSUTF8StringEncoding]];
            }
        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    return body;
}





@end
