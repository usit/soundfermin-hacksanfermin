//
//  RequestUtilsEventsList.m
//  SoundFermin
//
//  Created by Usue on 5/6/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import "RequestUtilsEventsList.h"
#import "ProcessResponseEventsList.h"

@implementation RequestUtilsEventsList

-(void)getRequest
{
    [[ViewCommonUtils sharedInstance] addSpinner];
    
    self.processResponse = [[ProcessResponseEventsList alloc] initWithConnectionError:YES
                                                                             delegate:self.delegate];
    [self getRequestWithUrl:[self urlModule:kEVENT_URL] auth:YES];
}

@end
