//
//  RequestUtilsEventsList.h
//  SoundFermin
//
//  Created by Usue on 5/6/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import "RequestUtils.h"

@interface RequestUtilsEventsList : RequestUtils

-(void)getRequest;

@end
