//
//  BaseUtils.h
//
//  Created by Usue Napal on 20/05/15
//  Copyright (c) 2013 Usit Development All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseUtils : NSObject

@property(nonatomic, strong) NSArray *requiredFields;
@property(nonatomic, strong) UIViewController *delegate;
@property(nonatomic, strong) UIRefreshControl *refreshControl;

-(id)initWithDelegate:(UIViewController*)delegate;

-(void)getData;
-(void)getDataSucceed:(NSMutableDictionary*)data;

-(void)postData;
-(void)postedDataSucceed:(NSMutableDictionary*)data;

-(void)connectionError;
-(void)addRefreshControlToTable:(UITableView*)table;

#pragma mark - Helpers
-(BOOL)requiredFieldsAreEmpty;
-(void)setColorForValidField:(UIView*)view;
-(void)setColorForInvalidField:(UIView*)view;

@end
