//
//  ViewCommonUtils.h
//
//  Created by Usue on 3/6/15.
//  Copyright (c) 2015 Usit Development. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ViewCommonUtils : NSObject

+(ViewCommonUtils *)sharedInstance;

////////////////////////////////////

#pragma mark - View Utils
-(void)addSpinner;
-(void)addSpinnerWithMessage:(NSString*)message;
-(void)removeSpinner;
-(void)showOneButtonAlert:(id)target title:(NSString*)title msg:(NSString*)msg buttonTitle:(NSString*)bTitle;
-(void)showTwoButtonsAlert:(id)target title:(NSString*)title msg:(NSString*)msg buttonCancel:(NSString*)cancelTitle otherButton:(NSString*)otherButtonTitle;
+(void)setTextFieldsPadding:(UITextField*)view;

#pragma mark - Nav Bar Utils
+(void)setNavBarTitle:(NSString*)title toController:(UIViewController*)controller;
+(void)setNavBarBackgroundColor:(UIColor*)color forController:(UINavigationController*)controller;
+(void)setRightButtonImage:(NSString*)img withSelector:(SEL)selector inTarget:(id)target;
+(void)setLeftButtonImage:(NSString*)img withSelector:(SEL)selector inTarget:(id)target;
+(void)removeLeftButton:(id)target;
+(void)removeRightButton:(id)target;
+(UIBarButtonItem*)createButtonItemWithImage:(NSString*)img selector:(SEL)selector inTarget:(id)target;
+(void)setBackButton:(UIViewController*)vc;

#pragma mark - Animation Utils
+(void)commitAnimationFor:(UIView*)view :(NSString *const)type :(NSString *const)subtype :(double)time :(CGRect)rect;
+(void)commitAnimationFor:(UIView*)view :(double)time :(CGRect)rect :(int)type;
+(void)commitBounceAnimation:(NSArray*)array;

#pragma mark - Views adaptation
+(void)adaptView:(UIView*)view toTopView:(UIView*)top margin:(float)margin;
+(void)adaptScrollContent:(UIScrollView*)view toLastView:(UIView*)last;
+(void)adaptViewHeight:(UIView*)view toLastView:(UIView*)last;
+(void)adaptLabelRight:(UILabel*)viewR toLabelLeft:(UILabel*)viewL;
+(void)adaptViewLeft:(UIView*)viewL toViewRight:(UIView*)viewR margin:(int)margin;
+(void)adaptLabelWidth:(UILabel*)label toCenterInSuperview:(UIView*)view;
+(void)setCenterOf:(UIView*)label accordingTo:(UIView*)view;
+(void)adaptLabelHeight:(UILabel*)label;
+(CGRect)adaptLabelRect:(UILabel*)label;
+(void)adaptLabelHeightWithoutMax:(UILabel*)label;
+(void)adaptTextViewHeight:(UITextView*)view;

#pragma mark - Format text Utils
+(void)setAttributtedTextToLabel:(UILabel*)label withColor:(UIColor*)color separator:(NSString*)separator;
+(void)setTextAsLink:(UILabel*)label;
+(void)setText:(NSString*)linkText asLinkInLabel:(UILabel*)label;

#pragma mark - Shaped views Utils
+(void)setBorderColor:(UIColor*)color with:(float)width toView:(UIView*)view;
+(void)setCircleView:(UIView*)view;


@end
