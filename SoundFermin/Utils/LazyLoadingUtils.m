//
//  LazyLoadingUtils.m
//
//  Created by Usit Development.
//  Copyright (c) 2015 Usit Development. All rights reserved.
//

#import "LazyLoadingUtils.h"
#import "RequestUtils.h"

@implementation LazyLoadingUtils

-(NSData*)makeRequest:(NSString*)url
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    return [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
}


#pragma mark - Get & Save data from User Defaults

-(void)saveImagesOnDefaults:(NSString*)hash :(NSData*)dataImg :(NSString*)key
{
    NSMutableDictionary *cachedImages = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:key]];

    if (cachedImages==nil)
        cachedImages = [[NSMutableDictionary alloc] init];
    
    if(dataImg){
        [cachedImages setObject:dataImg forKey:hash];
        [[NSUserDefaults standardUserDefaults] setObject:cachedImages forKey:key];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(NSData*)getImagesFromDefaults:(NSString*)hash :(NSString*)key
{
    NSMutableDictionary *cachedImages = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:key]];
    
    if (cachedImages==nil)
        return nil;
    else
        return [cachedImages objectForKey:hash];
}


#pragma mark - Download Image

-(NSData*)requestImage:(NSString*)url :(NSString*)key
{
    NSData *dataImg = [self makeRequest:url];
    
    if (dataImg==nil) {
        UIImage *defaultImg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"no_photo.png" ofType:nil]];
        dataImg = UIImageJPEGRepresentation(defaultImg, 1.0);
    }
    
    [self saveImagesOnDefaults:url :dataImg :key];
    
    return dataImg;
}


#pragma mark - Cell

-(void)setCellImage:(NSString*)url :(NSString*)key :(UIImageView*)imageView :(UITableView*)tableView :(NSIndexPath*)indexPath :(UIActivityIndicatorView*)actView
{
    [imageView setImage:nil];
    NSString *urlImg = [CommonUtils checkEmptyObjects:url];
    NSData *dataImg = [self getImagesFromDefaults:urlImg :key];
    
    if (dataImg==nil) {
        
        [actView setHidden:NO];
        [actView startAnimating];
        
        NSArray *data = [NSArray arrayWithObjects:urlImg, indexPath, key, tableView, [NSNumber numberWithInteger:imageView.tag],[NSNumber numberWithInteger:actView.tag], nil];
        [NSThread detachNewThreadSelector:@selector(getImageCellFrom:) toTarget:self withObject:data];
    } else {
        
        [actView setHidden:YES];
        [imageView setImage:[UIImage imageWithData:dataImg]];
        
        if ([imageView image]==nil)
            [imageView setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"avatar_default.jpg" ofType:nil]]];
    }
}

-(void)getImageCellFrom:(NSArray*)data
{
    @autoreleasepool {
        
        NSString *url = [data objectAtIndex:0];                          //URL to download Image
        NSIndexPath *index = [data objectAtIndex:1];                     //Cell Index to configure
        NSString *key = [data objectAtIndex:2];                          //Key for User Defaults
        UITableView *table = [data objectAtIndex:3];                     //TableView with Lazy Loading
        //NSUInteger cellTagImage=[[data objectAtIndex:4] intValue];     //Cell Tag to set image in the cell
        //NSUInteger cellTagActView=[[data objectAtIndex:5] intValue];   //Cell Tag to stop activity view in cell
        
        NSData *dataImg = [self requestImage:url :key];
        [self performSelectorOnMainThread:@selector(setImageInCell:) withObject:[NSArray arrayWithObjects:dataImg,index,table,[data objectAtIndex:4],[data objectAtIndex:5], nil] waitUntilDone:NO];
    }
}

-(void)setImageInCell:(NSArray*)array
{
    @try {
        
        NSData *dataImg = [array objectAtIndex:0];
        NSIndexPath *index = [array objectAtIndex:1];
        UITableView *table = [array objectAtIndex:2];
        NSUInteger cellTagImage = [[array objectAtIndex:3] intValue];
        NSUInteger cellTagActView = [[array objectAtIndex:4] intValue];
        
        if ([table numberOfRowsInSection:index.section]>index.row) {
            
            UITableViewCell *cell = [table cellForRowAtIndexPath:index];
            UIImageView *imageViewThumb = (UIImageView*)[cell.contentView viewWithTag:cellTagImage];
            
            [imageViewThumb setImage:[UIImage imageWithData:dataImg]];
            
            UIActivityIndicatorView *actView = (UIActivityIndicatorView*)[cell.contentView viewWithTag:cellTagActView];
            [actView setHidden:YES];
            [actView stopAnimating];
        }
    } @catch (NSException *exception) {}
    @finally {}
}


#pragma mark - ImageView

-(void)setViewImage:(NSString*)url :(NSString*)key :(UIImageView*)imageView :(UIActivityIndicatorView*)actView
{
    NSString *urlImg = [CommonUtils checkEmptyObjects:url];
    NSData *dataImg = [self getImagesFromDefaults:urlImg :key];
    
    if (dataImg==nil) {
        
        [actView setHidden:NO];
        [actView startAnimating];
        
        NSArray *data = [NSArray arrayWithObjects:urlImg, key, imageView, actView, nil];
        [NSThread detachNewThreadSelector:@selector(getImageViewFrom:) toTarget:self withObject:data];
    } else {
        
        [actView setHidden:YES];
        [imageView setImage:[UIImage imageWithData:dataImg]];
    }
    if ([imageView image]==nil)
        [imageView setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"no_photo_white.png" ofType:nil]]];
}

-(void)getImageViewFrom:(NSArray*)data
{
    @autoreleasepool {
        
        NSString *url = [data objectAtIndex:0];                          //URL to download Image
        NSString *key = [data objectAtIndex:1];                          //Key for User Defaults
        UIImageView *imageView = [data objectAtIndex:2];                 //ImageView with Lazy Loading
        UIActivityIndicatorView *actView = [data objectAtIndex:3];   //Tag to stop activity view in cell
        
        NSData *dataImg;
        
        if (url!=nil) {
            dataImg = [self requestImage:url :key];
        }
        
        [self performSelectorOnMainThread:@selector(setImageInImageView:) withObject:[NSArray arrayWithObjects:dataImg,imageView,actView, nil] waitUntilDone:NO];
    }
}

-(void)setImageInImageView:(NSArray*)array
{
    @try {
        
        NSData *dataImg = [array objectAtIndex:0];
        UIImageView *imageView = [array objectAtIndex:1];
        UIActivityIndicatorView *actView = [array objectAtIndex:2];
        
        [imageView setImage:[UIImage imageWithData:dataImg]];
        [actView setHidden:YES];
        [actView stopAnimating];
    } @catch (NSException *exception) {}
    @finally {}
}


#pragma mark - Clean Unused Cache

+(void)cleanUnusedCache:(NSString*)key :(NSArray*)arrayKeys
{
    NSMutableDictionary *dictCache = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:key]];
    NSArray *aux = [NSArray arrayWithArray:[dictCache allKeys]];
    
    for (NSString *ckey in aux) {
        if (![arrayKeys containsObject:ckey])
            [dictCache removeObjectForKey:ckey];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:dictCache forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
