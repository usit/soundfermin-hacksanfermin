//
//  ShareUtils.m
//
//  Created by Usue on 01/09/14.
//  Copyright (c) 2014 Usue Napal. All rights reserved.
//

#import "ShareUtils.h"
#import "Event.h"

@interface ShareUtils()

@property(nonatomic, strong) Event *event;
@property(nonatomic, strong) UIViewController *delegate;

@end

@implementation ShareUtils


-(id)initWithDelegate:(UIViewController *)delegate event:(Event*)event
{
    self = [super init];
    
    if (self) {
        self.event = event;
        self.delegate = delegate;
    }
    
    return self;
}


#pragma mark - UIActionSheetDelegate

-(void)openShareController
{
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:@[[self getShareMessage]] applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypeAddToReadingList, UIActivityTypeAirDrop, UIActivityTypeAssignToContact, UIActivityTypePostToFlickr, UIActivityTypePostToTencentWeibo, UIActivityTypePostToVimeo, UIActivityTypePostToWeibo, UIActivityTypePrint, UIActivityTypeSaveToCameraRoll, UIActivityTypeCopyToPasteboard];
    
    if (iOS_VERSION_GREATER_OR_EQUAL_TO(@"8.0")) {
        activityVC.popoverPresentationController.sourceView = self.delegate.view.superview;
    }
    
    [self.delegate presentViewController:activityVC animated:YES completion:nil];
}


#pragma mark - Sharing Helpers

-(NSString*)getShareMessage
{
    NSString *date = [DateTimeUtils convertServerDate:self.event.date];
    NSString *shareStr1 = [CommonUtils localizedStrFor:@"share_igo"];
    NSString *shareStr2 = [CommonUtils localizedStrFor:@"share_igo2"];
    
    return [NSString stringWithFormat:@"%@ %@ - %@ %@", shareStr1, self.event.name, date, shareStr2];
}

@end
