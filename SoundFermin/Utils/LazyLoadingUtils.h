//
//  LazyLoadingUtils.h
//
//  Created by Usit Development.
//  Copyright (c) 2015 Usit Development. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LazyLoadingUtils : NSObject

#pragma mark - Get & Save data from User Defaults
-(void)saveImagesOnDefaults:(NSString*)hash :(NSData*)dataImg :(NSString*)key;
-(NSData*)getImagesFromDefaults:(NSString*)hash :(NSString*)key;

#pragma mark - Function for Cells
-(void)setCellImage:(NSString*)url :(NSString*)key :(UIImageView*)imageView :(UITableView*)tableView :(NSIndexPath*)indexPath :(UIActivityIndicatorView*)actView;
-(void)getImageCellFrom:(NSArray*)data;

#pragma mark - ImageView
-(void)setViewImage:(NSString*)url :(NSString*)key :(UIImageView*)imageView :(UIActivityIndicatorView*)actView;
-(void)getImageViewFrom:(NSArray*)data;
-(void)setImageInImageView:(NSArray*)array;

#pragma mark - Clean Unused Cache
+(void)cleanUnusedCache:(NSString*)key :(NSArray*)arrayKeys;

@end
