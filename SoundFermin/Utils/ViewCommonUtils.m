//
//  ViewCommonUtils.m
//
//  Created by Usue on 3/6/15.
//  Copyright (c) 2015 Usit Development. All rights reserved.
//

#import "ViewCommonUtils.h"

static ViewCommonUtils *sharedInstance = nil;

@implementation ViewCommonUtils

/*
 * Singleton
 */
+ (ViewCommonUtils *)sharedInstance {
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [[self alloc] init];
        }
    }
    
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [super allocWithZone:zone];
            return sharedInstance;  // assignment and return on first allocation
        }
    }
    
    return nil; //on subsequent allocation attempts return nil
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

#pragma mark - View Utils

-(void)addSpinner
{
    [self addSpinnerWithMessage:@"loading"];
}

-(void)addSpinnerWithMessage:(NSString*)message
{
    UIView *currentView = [[[[[UIApplication sharedApplication] windows] objectAtIndex:0] rootViewController] view];
    
    for (UIView *spinner in [currentView subviews]) {
        if (([[spinner class] isSubclassOfClass:[UIView class]])&&([spinner tag]==SPINNER_TAG))
            [spinner setHidden:YES];
    }
    
    int width = 140, height = 70;
    CGRect screen = [currentView bounds];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake((screen.size.width/2)-(width/2), (screen.size.height/2)-(height/2), width, height)];
    [view setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
    [[view layer] setCornerRadius:8.0f];
    [view setTag:SPINNER_TAG];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 30)];
    [label setFont:[UIFont fontWithName:@"Helvetica" size:12]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTextColor:[UIColor whiteColor]];
    [label setText:[CommonUtils localizedStrFor:message]];
    
    UIActivityIndicatorView *actView=[[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((width/2)-10, (height/2), 20, 20)];
    [actView startAnimating];
    
    [view addSubview:actView];
    [view addSubview:label];
    [currentView addSubview:view];
}

-(void)removeSpinner
{
    UIView *currentView = [[[[[UIApplication sharedApplication] windows] objectAtIndex:0] rootViewController] view];
    
    for (UIView *spinner in [currentView subviews]) {
        if (([[spinner class] isSubclassOfClass:[UIView class]])&&([spinner tag]==SPINNER_TAG))
            [spinner removeFromSuperview];
    }
}


-(void)showOneButtonAlert:(id)target title:(NSString*)title msg:(NSString*)msg buttonTitle:(NSString*)bTitle
{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:[CommonUtils localizedStrFor:title]
                          message:[CommonUtils localizedStrFor:msg]
                          delegate:target
                          cancelButtonTitle:[CommonUtils localizedStrFor:bTitle]
                          otherButtonTitles:nil];
    [alert show];
}

-(void)showTwoButtonsAlert:(id)target title:(NSString*)title msg:(NSString*)msg buttonCancel:(NSString*)cancelTitle otherButton:(NSString*)otherButtonTitle
{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:[CommonUtils localizedStrFor:title]
                          message:[CommonUtils localizedStrFor:msg]
                          delegate:target
                          cancelButtonTitle:[CommonUtils localizedStrFor:cancelTitle]
                          otherButtonTitles: [CommonUtils localizedStrFor:otherButtonTitle],nil];
    [alert show];
}

+(void)setTextFieldsPadding:(UITextField*)view
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    view.leftView = paddingView;
    view.leftViewMode = UITextFieldViewModeAlways;
}


#pragma mark - Nav Bar Utils

+(void)setNavBarTitle:(NSString*)title toController:(UIViewController*)controller
{
    [controller.navigationItem setTitle:[CommonUtils localizedStrFor:title]];
    [controller.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"MarkerFelt-Thin" size:24.0]}];
    [self setNavBarBackgroundColor:SF_NAVBAR_COLOR forController:controller.navigationController];
}

+(void)setNavBarBackgroundColor:(UIColor*)color forController:(UINavigationController*)controller
{
    [controller.navigationBar setTranslucent:YES];
    [controller.navigationBar setBarTintColor:color];
    [controller.navigationBar setTintColor:[UIColor whiteColor]];
}

+(void)setRightButtonImage:(NSString*)img withSelector:(SEL)selector inTarget:(id)target
{
    [[target navigationItem] setRightBarButtonItem:[self createButtonItemWithImage:img selector:selector inTarget:target]];
}

+(void)setLeftButtonImage:(NSString*)img withSelector:(SEL)selector inTarget:(id)target
{
    [[target navigationItem] setLeftBarButtonItem:[self createButtonItemWithImage:img selector:selector inTarget:target]];
}

+(void)removeLeftButton:(id)target
{
    [[target navigationItem] setLeftBarButtonItem:nil];
}

+(void)removeRightButton:(id)target
{
    [[target navigationItem] setRightBarButtonItem:nil];
}

+(UIBarButtonItem*)createButtonItemWithImage:(NSString*)img selector:(SEL)selector inTarget:(id)target
{
    UIButton *addButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 5, 30, 30)];
    [addButton setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:img ofType:@"png"]] forState:UIControlStateNormal];
    [addButton addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    return [[UIBarButtonItem alloc] initWithCustomView:addButton];
}

+(void)setBackButton:(UIViewController*)vc
{
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[CommonUtils localizedStrFor:@"back"] style:UIBarButtonItemStylePlain target:vc.navigationController action:@selector(popViewControllerAnimated:)];
    
    NSString *resource = [[NSBundle mainBundle] pathForResource:@"header-back@2x" ofType:@"png"];
    UIImage *image = [UIImage imageWithContentsOfFile:resource];
    
    [backButton setBackgroundImage:image forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [vc.navigationItem setLeftBarButtonItem:backButton];
}

#pragma mark - Animation Utils

+(void)commitAnimationFor:(UIView*)view :(NSString *const)type :(NSString *const)subtype :(double)time :(CGRect)rect
{
    CATransition *animation = [CATransition animation];
    [animation setDuration:time];
    [animation setType:type];
    [animation setSubtype:subtype];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    
    view.frame = rect;
    [[view layer] addAnimation:animation forKey:@""];
}

+(void)commitAnimationFor:(UIView*)view :(double)time :(CGRect)rect :(int)type
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:time];
    [UIView setAnimationCurve:type];
    
    [view setFrame:rect];
    [UIView commitAnimations];
}

+(void)commitBounceAnimation:(NSArray*)array
{
    UIView *view = [array objectAtIndex:0];
    double duration = [[array objectAtIndex:1] doubleValue];
    double repeatCount = [[array objectAtIndex:2] doubleValue];
    NSNumber *from = [array objectAtIndex:3];
    NSNumber *to = [array objectAtIndex:4];
    
    CABasicAnimation *bounceAnimation = [CABasicAnimation animationWithKeyPath:@"position.x"];
    bounceAnimation.duration = duration;
    bounceAnimation.fromValue = from;
    bounceAnimation.toValue = to;
    bounceAnimation.repeatCount = repeatCount;
    bounceAnimation.autoreverses = YES;
    bounceAnimation.fillMode = kCAFillModeForwards;
    bounceAnimation.removedOnCompletion = NO;
    bounceAnimation.additive = YES;
    [view.layer addAnimation:bounceAnimation forKey:@"bounceAnimation"];
}


#pragma mark - Views adaptation

+(void)adaptView:(UIView*)view toTopView:(UIView*)top margin:(float)margin
{
    CGRect rect = view.frame;
    rect.origin.y = top.frame.origin.y + top.frame.size.height + margin;
    [view setFrame:rect];
}

+(void)adaptScrollContent:(UIScrollView*)view toLastView:(UIView*)last
{
    CGSize size = view.contentSize;
    size.height = last.frame.origin.y + last.frame.size.height + 20;
    [view setContentSize:size];
}

+(void)adaptViewHeight:(UIView*)view toLastView:(UIView*)last
{
    CGRect frame = view.frame;
    frame.size.height = last.frame.origin.y + last.frame.size.height;
    [view setFrame:frame];
}

+(void)adaptLabelRight:(UILabel*)viewR toLabelLeft:(UILabel*)viewL
{
    CGRect frame = viewL.frame;
    
    if (viewL.layer.cornerRadius==0) {
        frame.size.width = [viewL.text sizeWithAttributes:@{NSFontAttributeName:viewL.font}].width;
        [viewL setFrame:frame];
    }
    
    frame = viewR.frame;
    frame.origin.x = viewL.frame.origin.x + viewL.frame.size.width + 2;
    
    if (viewR.layer.cornerRadius==0) {
        frame.size.width = viewR.superview.frame.size.width - viewL.frame.size.width - viewL.frame.origin.x;
    }
    
    [viewR setFrame:frame];
    
    [[viewR layer] setBorderColor:viewR.backgroundColor.CGColor];
    [[viewR layer] setBorderWidth:1];
}

+(void)adaptViewLeft:(UIView*)viewL toViewRight:(UIView*)viewR margin:(int)margin
{
    CGRect rect = viewL.frame;
    rect.origin.x = viewR.frame.origin.x - rect.size.width - margin;
    [viewL setFrame:rect];
}

+(void)adaptLabelWidth:(UILabel*)label toCenterInSuperview:(UIView*)view
{
    CGRect frame = label.frame;
    frame.size.width = [self adaptLabelRect:label].size.width;
    [label setFrame:frame];
    
    [self setCenterOf:label accordingTo:view];
}

+(void)setCenterOf:(UIView*)label accordingTo:(UIView*)view
{
    CGPoint center = label.center;
    center.x = view.center.x;
    [label setCenter:center];
}

+(void)adaptLabelHeight:(UILabel*)label
{
    CGRect frame = label.frame;
    frame.size.height = [self adaptLabelRect:label].size.height;
    
    [label setFrame:frame];
}

+(CGRect)adaptLabelRect:(UILabel*)label
{
    return [label.text boundingRectWithSize:label.frame.size
                                    options:NSStringDrawingUsesLineFragmentOrigin
                                 attributes:@{NSFontAttributeName:label.font}
                                    context:nil];
}

+(void)adaptLabelHeightWithoutMax:(UILabel*)label
{
    CGRect frame = label.frame;
    frame.size.height = [label.text boundingRectWithSize:CGSizeMake(frame.size.width, 1000)
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:label.font}
                                                 context:nil].size.height;
    
    [label setFrame:frame];
    [[label layer] setBorderColor:label.backgroundColor.CGColor];
    [[label layer] setBorderWidth:1];
}

+(void)adaptTextViewHeight:(UITextView *)view
{
    [view setScrollEnabled:YES];
    [view sizeToFit];
    [view setScrollEnabled:NO];
}


#pragma mark - Format text Utils

+(void)setAttributtedTextToLabel:(UILabel*)label withColor:(UIColor*)color separator:(NSString*)separator
{
    NSString *text = label.text;
    NSString *fontName = [[label.font fontDescriptor] objectForKey:@"NSFontNameAttribute"];
    UIFont *font = [UIFont fontWithName:fontName size:label.font.pointSize];
    
    NSRange range = [text rangeOfString:separator];
    int indexToSeparate = (int)range.location;
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text];
    [attributedText addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, indexToSeparate)];
    [attributedText addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0, indexToSeparate)];
    [attributedText addAttribute:NSFontAttributeName value:label.font range:NSMakeRange(indexToSeparate, (text.length - indexToSeparate))];
    label.attributedText = attributedText;
}

+(void)setTextAsLink:(UILabel*)label
{
    NSString *text = label.text;
    NSString *fontName = [[label.font fontDescriptor] objectForKey:@"NSFontNameAttribute"];
    UIFont *font = [UIFont fontWithName:fontName size:label.font.pointSize];
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text];
    [attributedText addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, text.length)];
    [attributedText addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0, text.length)];
    label.attributedText = attributedText;
}

+(void)setText:(NSString*)linkText asLinkInLabel:(UILabel*)label
{
    NSString *text = label.text;
    NSString *fontName = [[label.font fontDescriptor] objectForKey:@"NSFontNameAttribute"];
    UIFont *font = [UIFont fontWithName:fontName size:label.font.pointSize];
    NSRange range = [text rangeOfString:linkText];
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text];
    [attributedText addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, range.location)];
    [attributedText addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:range];
    label.attributedText = attributedText;
}

#pragma mark - Shaped views Utils

+(void)setBorderColor:(UIColor *)color with:(float)width toView:(UIView *)view
{
    [view.layer setBorderColor:[color CGColor]];
    [view.layer setBorderWidth:width];
}

+(void)setCircleView:(UIView*)view
{
    view.layer.masksToBounds = YES;
    [[view layer] setCornerRadius:view.frame.size.height/2 - 1];
}

@end
