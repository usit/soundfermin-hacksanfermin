//
//  ShareUtils.h
//
//  Created by Usue on 01/09/14.
//  Copyright (c) 2014 Usue Napal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>
#import "Event.h"

@interface ShareUtils : NSObject <UIActionSheetDelegate>

@property(nonatomic, strong) NSString *answerToShare;

-(id)initWithDelegate:(UIViewController*)delegate event:(Event*)event;
-(void)openShareController;

@end
