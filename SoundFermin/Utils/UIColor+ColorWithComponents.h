//
//  UIColor+ColorWithComponents.h
//
//  Created by Usue on 3/6/15.
//  Copyright (c) 2015 Usit Development. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ColorWithComponents)

+(UIColor*)colorWithHexRed:(int)red green:(int)green blue:(int)blue alpha:(int)alpha;
+(UIColor*)colorWithARGBInt:(uint)color;

@end