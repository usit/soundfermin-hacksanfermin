//
//  CommonUtils.m
//
//  Created by Usue on 3/6/15.
//  Copyright (c) 2015 Usit Development. All rights reserved.
//

#import "CommonUtils.h"

@implementation CommonUtils

#pragma mark Common

+(NSString*)getDeviceLanguage
{
    NSString *appLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:@"appLanguage"];
    
    if (appLanguage) {
        return appLanguage;
    } else {
        NSArray *languages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
        return [languages objectAtIndex:0];
    }
}

+(void)setDeviceLanguage:(NSString *)language
{
    [[NSUserDefaults standardUserDefaults] setObject:language forKey:@"appLanguage"];
}

+(NSBundle*)getLanguageBundle
{
    NSString *currentLanguage = [self getDeviceLanguage];
    
    if ((![currentLanguage isEqualToString:@"es"]) && (![currentLanguage isEqualToString:@"en"]) && (![currentLanguage isEqualToString:@"eu"])) {
        currentLanguage = @"en";
    }
    
    NSString *path = [[NSBundle mainBundle] pathForResource:currentLanguage ofType:@"lproj"];
    NSBundle *languageBundle = [NSBundle bundleWithPath:path];
    
    return languageBundle;
}

+(NSString*)localizedStrFor:(NSString*)str
{
    return [[self getLanguageBundle] localizedStringForKey:str value:nil table:nil];
}

+(id)checkEmptyObjects:(id)text
{
    if ([[text class] isSubclassOfClass:[NSString class]]) {
        if ((text==NULL) || (text==nil)) {
            return @"";
        }
    } else if (([text class]==nil) || ([[text class] isSubclassOfClass:[NSNull class]])) {
        text=[[NSString alloc]init];
        text=@"";
    }
    
    return text;
}

+(BOOL)isEmpty:(id)text
{
    return [[self checkEmptyObjects:text] length]==0;
}

+(void)addObjectUnique:(id)object inArray:(NSMutableArray*)array
{
    if ([array containsObject:object]) {
        return;
    }
    
    [array addObject:object];
}

+(MKCoordinateRegion)coordinateRegionForCoordinates:(CLLocationCoordinate2D*)coords coordCount:(int)coordCount
{
    MKMapRect r = MKMapRectNull;
    
    for (NSUInteger i=0; i < coordCount; i++) {
        MKMapPoint p = MKMapPointForCoordinate(coords[i]);
        r = MKMapRectUnion(r, MKMapRectMake(p.x, p.y, 0, 0));
    }
    
    return MKCoordinateRegionForMapRect(r);
}

@end
