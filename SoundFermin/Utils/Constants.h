//
//  Constants.h
//
//  Created by Usue on 3/6/15.
//  Copyright (c) 2015 Usit Development. All rights reserved.
//

@interface Constants : NSObject

#pragma mark - URLS

#define SERVER_BASE_URL @"http://soundfermin.usitdevelopment.com/web/api"

#define kEVENT_URL             @"/sfevents"
#define kCURRENT_EVENTS_URL    @"/sfcurrentEvents"

#pragma mark - RESPONSE PARAMS

#define kCONTENT            @"content"
#define kCONTENT_EVENTS     @"events"
#define kSUCCESS            @"ok"
#define kSTATUS             @"status"


#pragma mark - USER DEFAULTS CONSTANTS

#define kUD_CACHE_USERS             @"UDcacheUsers"
#define kUD_LAST_EVENTS_DOWNLOAD    @"UDlastEventsDownload"
#define kUD_EVENTS_DATA             @"UDeventsData"


#pragma mark - MACROS

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define BOUNDS_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define BOUNDS_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define BOUNDS_MAX_LENGTH (MAX(BOUNDS_WIDTH, BOUNDS_HEIGHT))
#define BOUNDS_MIN_LENGTH (MIN(BOUNDS_WIDTH, BOUNDS_HEIGHT))

#define NAVBAR_STATUS_HEIGHT 64

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && BOUNDS_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && BOUNDS_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && BOUNDS_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && BOUNDS_MAX_LENGTH == 736.0)

#define iOS_VERSION_GREATER_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


#pragma mark - CONSTANTS

#define SPINNER_TAG         888
#define PNG                 @"png"
#define SERVER_DATE_FORMAT  @"yyyy-MM-dd HH:mm:ss"
#define APP_DATE_FORMAT     @"dd MMM yyyy"


#pragma mark - COLORS

#define SF_LIGHT_GRAY      [UIColor colorWithARGBInt:0xfff1f1f1]
#define SF_DARK_GRAY       [UIColor colorWithARGBInt:0xff555555]
#define SF_NAVBAR_COLOR    [UIColor colorWithARGBInt:0xff000000]
#define SF_RED             [UIColor colorWithARGBInt:0xffe74c3c]

#pragma mark - FONTS

#define BASE_FONT_BOLD_(s) [UIFont fontWithName:@"HelveticaNeue-Bold" size:s]
#define BASE_FONT_LIGHT_(s) [UIFont fontWithName:@"HelveticaNeue-Light" size:s]
#define BASE_FONT_REGULAR_(s) [UIFont fontWithName:@"HelveticaNeue" size:s]



#pragma mark - DATE TIME

#define YEAR_INTERVAL       31536000
#define DAY_INTERVAL        86400
#define HOUR_INTERVAL       3600
#define HALFHOUR_INTERVAL   1800
#define QUARTER_INTERVAL    900


#pragma mark - NOTIFS 

#define NOTIF_HANDLE_MENU   @"handleMenu"
#define NOTIF_SHOW_MAP      @"showMap"
#define NOTIF_SHOW_EVENTS   @"showEvents"
#define NOTIF_SHOW_ABOUT    @"showAbout"
#define NOTIF_SHOW_SHARE    @"showShare"



@end
