//
//  DateTimeUtils.m
//  SoundFermin
//
//  Created by Itziar on 5/6/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import "DateTimeUtils.h"

@implementation DateTimeUtils

#pragma mark - Date

+(NSString*)convertServerDate:(NSString*)date
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:SERVER_DATE_FORMAT];
    NSDate *newDate = [df dateFromString:date];
    
    [df setDateFormat:APP_DATE_FORMAT];
    NSString *strDate = [df stringFromDate:newDate];
    
    return strDate;
}

+(NSString*)getDayFromDate:(NSString*)date
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:SERVER_DATE_FORMAT];
    NSDate *newDate = [df dateFromString:date];
    
    [df setDateFormat:@"d"];
    NSString *day = [df stringFromDate:newDate];
    
    return day;
}


#pragma mark Date Utils

+(NSString*)calculateTimeToDateEvent:(NSString*)date
{
    NSDate *currentDate = [NSDate date];
    NSTimeInterval timeInterval = [[self getDateFromString:date] timeIntervalSinceDate:currentDate];
    int minutes = timeInterval/60;
    int hours = minutes/60; minutes = minutes%60;
    
    NSString *minutesStr = @"map_pin_minutes", *hoursStr = @"map_pin_hours";
    if (hours==0) {
        return [CommonUtils localizedStrFor:@"map_pin_now"];
    }else if ((hours>0)&&(minutes>0)){
        minutesStr = @"map_pin_minutes_abr"; hoursStr = @"map_pin_hours_abr";
    }else if ((hours<0)||(minutes<0)){
        return @"";
    }
    
    hoursStr = (hours==0)? @"": [NSString stringWithFormat:@"%i%@", (int)hours, [CommonUtils localizedStrFor:hoursStr]];
    minutesStr = (minutes==0)? @"": [NSString stringWithFormat:@"%i%@", (int)minutes, [CommonUtils localizedStrFor:minutesStr]];
    
    return [NSString stringWithFormat:@"%@%@", hoursStr, minutesStr];
}

+(NSString*)getStringFromDate:(NSDate*)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:SERVER_DATE_FORMAT];
    return [formatter stringFromDate:date];
}

+(NSDate*)getDateFromString:(NSString*)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:SERVER_DATE_FORMAT];
    return [formatter dateFromString:date];
}

+(NSString*)getDate:(NSString*)date
{
    return [DateTimeUtils convertServerDate:date];
}

+(NSString*)getTime:(NSString*)date
{
    NSString *time = @"";
    NSArray *dateComponents = [date componentsSeparatedByString:@" "];
    
    if (dateComponents.count > 1) {
        return [[dateComponents objectAtIndex:1] substringToIndex:5];
    }
    
    return time;
}

@end
