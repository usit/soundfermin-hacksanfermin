//
//  AppDelegate.h
//  SoundFermin
//
//  Created by Usue on 3/6/15.
//  Copyright (c) 2015 UsIt Development. All rights reserved.
//

#import "MenuViewController.h"
#import "MainViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) MainViewController *mainView;
@property (strong, nonatomic) MenuViewController *menuView;
@property (strong, nonatomic) UINavigationController *navController;

@end

